<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;

$console = new Application('Sam', 'n/a');
$console
    ->register('so:process')
    ->setDefinition(array())
    ->setDescription('Processes SO Queue')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $table = new Table($output);

        while(1) {
            list(, $message_id) = $app['predis']->blpop("queue:message", 10);
            if(!$message_id) continue;

            $message = $app['predis']->hgetall("message:$message_id");
            $so = json_decode($message['message_data']);
            $token = $app['auth_token']($so);

            $app['add_mo']($so->msisdn, $so->operatorid, $so->shortcodeid, $so->text, $token, date("Y-m-d H:i:s"));

            $table->setRow(0, array($message_id, $message['message_data']));
            $table->render();
        }
    });

$console
    ->register('so:unprocessed')
    ->setDefinition(array())
    ->setDescription('Shows # of unprocessed MOs')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        echo $app['predis']->llen("queue:message") . PHP_EOL;
    });

$console
    ->register('so:clear-unprocessed')
    ->setDefinition(array())
    ->setDescription('Clears unprocessed MOs')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        echo 'Deleting ' . $app['predis']->llen("queue:message") . PHP_EOL;

        $app['predis']->del("queue:message");
        shell_exec('redis-cli KEYS "message:*" | xargs redis-cli DEL');

        echo $app['predis']->llen("queue:message") . ' in queue now!' . PHP_EOL;
    });

return $console;