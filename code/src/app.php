<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Predis\Silex\ClientServiceProvider(), [
    'predis.parameters' => 'tcp://127.0.0.1:6379'
]);

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'    =>  'pdo_mysql',
        'dbname'    =>  'samtt',
        'host'      =>  'localhost',
        'user'      =>  'root',
        'password'  =>  '',
        'port'      =>  3306,
        'charset'   => 'utf8'
    ),
));

$app['auth_token'] = $app->protect(function ($params) {
    $arg = json_encode($params);
    $registermo_path = __DIR__ . '/../bin/registermo';
    return `$registermo_path $arg`;
});

$app['add_mo'] = $app->protect(function ($msisdn, $operatorid, $shortcodeid, $text, $token, $created_at) use ($app) {
    $app['db']->insert('mo', array(
        'msisdn'        =>  $msisdn,
        'operatorid'    =>  $operatorid,
        'shortcodeid'   =>  $shortcodeid,
        'text'          =>  $text,
        'auth_token'    =>  $token,
        'created_at'    =>  $created_at
    ));
});

$app['add_to_queue'] = $app->protect(function ($json) use ($app) {
    $message_id = $app['predis']->incr("all:mos");

    $message = array(
        "id"            => $message_id,
        "message_data"	=> $json,
        "datetime"      => date('Y-m-d h:i:s')
    );

    $app['predis']->hmset("message:$message_id", $message);
    $app['predis']->lpush("queue:message", $message_id);
});

$app['increment_latest_mo'] = $app->protect(function ($expire = 900) use ($app) {
    $uniqId = $app['predis']->incr('mo_count');
    $key = 'latest_mo:' . $uniqId;

    $app['predis']->multi();
    $app['predis']->rpush($key, date('Y-m-d h:i:s'));
    $app['predis']->expire($key, $expire);
    $app['predis']->exec();
});

$app['latest_mo_count'] = $app->share(function () use ($app) {
    return count($app['predis']->keys('latest_mo:*'));
});

$app['time_span'] = $app->protect(function ($last_n = 10000) use ($app) {
    $maxIndex = $app['predis']->get('all:mos');
    $minIndex = $maxIndex >= $last_n ? ($maxIndex - $last_n) : 1;

    $res = array();
    $res['max(created_at)'] = $app['predis']->hget("message:$maxIndex", 'datetime');
    $res['min(created_at)'] = $app['predis']->hget("message:$minIndex", 'datetime');

    return $res;
});

$app->get('/', function (Request $request) use ($app) {
    $app['add_to_queue'](json_encode($request->query->all()));
    $app['increment_latest_mo'](900);

    return new JsonResponse(array(
        'status' => 'ok'
    ));
});

$app->get('/last5', function (Request $request) use ($app) {
    $result = $app['db']->fetchAll('SELECT * FROM mo order by id desc limit 5');

    return new JsonResponse($result);
});

$app->get('/redis-status', function (Request $request) use ($app) {
    list(, $message_id) = $app['predis']->blpop("queue:message", 0);
    $messages = $app['predis']->hgetall("message:$message_id");

    return new JsonResponse($messages);
});

$app->get('/stats', function () use ($app) {
    $response = array();

    $response['last_15_min_mo_count'] = $app['latest_mo_count'];
    $response['time_span_last_10k'] = $app['time_span']();

    return new JsonResponse($response);
});

return $app;
